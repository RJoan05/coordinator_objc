//
//  BaseViewModel.h
//  MVVM_C
//
//  Created by Zoe.Lin on 2019/10/15.
//  Copyright © 2019 Zoe.Lin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
typedef void(^FinishBlock)(void);

@interface BaseViewModel : NSObject

@end

NS_ASSUME_NONNULL_END
