//
//  BaseViewController.h
//  MVVM_C
//
//  Created by Zoe.Lin on 2019/10/15.
//  Copyright © 2019 Zoe.Lin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewModel.h"

NS_ASSUME_NONNULL_BEGIN


@interface BaseViewController : UIViewController

@property (nonatomic, strong) BaseViewModel *viewModel;

- (instancetype)initWithViewModel:(BaseViewModel *)vm;

@end

NS_ASSUME_NONNULL_END
