//
//  ProductListViewController.h
//  MVVM_C
//
//  Created by Zoe.Lin on 2019/10/16.
//  Copyright © 2019 Zoe.Lin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "ProductListViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductListViewController : BaseViewController

@property (nonatomic, strong) ProductListViewModel *viewModel;
@property (nonatomic, copy) FinishBlock logoutAction;

@end

NS_ASSUME_NONNULL_END
