//
//  ProductListViewController.m
//  MVVM_C
//
//  Created by Zoe.Lin on 2019/10/16.
//  Copyright © 2019 Zoe.Lin. All rights reserved.
//

#import "ProductListViewController.h"
#import "ProductListViewModel.h"

@interface ProductListViewController ()<UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ProductListViewController

@synthesize viewModel = _viewModel;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initTableView];
    [self initViewModelFunction];
}

// MARK - Privite finction
- (void)initTableView {
    self.tableView.dataSource = self;
}

- (void)initViewModelFunction {
    __weak typeof(self) weakSelf = self;
    self.viewModel.finishFetchData = ^{
        [weakSelf.tableView reloadData];
    };
    
    [self.viewModel fetchData];
}

// MARK - table view delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.viewModel.items.count;
}

// MARK - Button Action
- (IBAction)clickLogoutButton:(id)sender {
    if (self.logoutAction) {
        self.logoutAction();
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


@end
