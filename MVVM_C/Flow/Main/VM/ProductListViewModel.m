//
//  ProductListViewModel.m
//  MVVM_C
//
//  Created by Zoe.Lin on 2019/10/16.
//  Copyright © 2019 Zoe.Lin. All rights reserved.
//

#import "ProductListViewModel.h"

@implementation ProductListViewModel

- (void)fetchData {
    // finish call API
    if (self.finishFetchData) {
        self.finishFetchData();
    }
}

@end
