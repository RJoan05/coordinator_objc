//
//  ProductListViewModel.h
//  MVVM_C
//
//  Created by Zoe.Lin on 2019/10/16.
//  Copyright © 2019 Zoe.Lin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProductListViewModel : BaseViewModel

@property (nonatomic, copy) FinishBlock finishFetchData;
@property (nonatomic, assign) NSMutableArray *items;

- (void)fetchData;

@end

NS_ASSUME_NONNULL_END
