//
//  LoginViewModel.h
//  MVVM_C
//
//  Created by Zoe.Lin on 2019/10/14.
//  Copyright © 2019 Zoe.Lin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LoginViewModel : BaseViewModel

@end

NS_ASSUME_NONNULL_END
