//
//  ForgetPasswordViewController.h
//  MVVM_C
//
//  Created by Zoe.Lin on 2019/10/15.
//  Copyright © 2019 Zoe.Lin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ForgetPasswordViewController : BaseViewController

@property (nonatomic, copy) FinishBlock sendPasswordAction;

@end

NS_ASSUME_NONNULL_END
