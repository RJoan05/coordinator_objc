//
//  LoginViewController.h
//  MVVM_C
//
//  Created by Zoe.Lin on 2019/10/14.
//  Copyright © 2019 Zoe.Lin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "LoginViewModel.h"


NS_ASSUME_NONNULL_BEGIN


@interface LoginViewController : BaseViewController

@property (nonatomic, copy) FinishBlock loginAction;
@property (nonatomic, copy) FinishBlock forgetPwAction;

@end

NS_ASSUME_NONNULL_END
