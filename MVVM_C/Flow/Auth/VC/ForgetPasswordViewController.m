//
//  ForgetPasswordViewController.m
//  MVVM_C
//
//  Created by Zoe.Lin on 2019/10/15.
//  Copyright © 2019 Zoe.Lin. All rights reserved.
//

#import "ForgetPasswordViewController.h"

@interface ForgetPasswordViewController ()

@end

@implementation ForgetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

// MARK - Button Action
- (IBAction)clickSendPasswordButton:(id)sender {
    if (self.sendPasswordAction) {
        self.sendPasswordAction();
    }
}


@end
