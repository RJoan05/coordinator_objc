//
//  LoginViewController.m
//  MVVM_C
//
//  Created by Zoe.Lin on 2019/10/14.
//  Copyright © 2019 Zoe.Lin. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

// MARK - Button Action
- (IBAction)clickLoginButton:(id)sender {
    if (self.loginAction) {
        self.loginAction();
    }
}

- (IBAction)clickForgetPasswordButton:(id)sender {
    if (self.forgetPwAction) {
        self.forgetPwAction();
    }
}

@end
