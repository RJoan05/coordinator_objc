//
//  BaseCoordinator.m
//  MVVM_C
//
//  Created by Zoe.Lin on 2019/10/8.
//  Copyright © 2019 Zoe.Lin. All rights reserved.
//

#import "BaseCoordinator.h"

@implementation BaseCoordinator

// MARK - Coordinator protocol
- (instancetype)initWithRootViewController:(UINavigationController *)rootController {
    self = [super init];
    self.rootController = rootController;
    return self;
}

- (void)start {
    [self start:0];
}

- (void)start:(DeepLinkOption)option {
    // Override
}

// MARK - Owen function
- (void)addDependency:(NSObject<Coordinator> *)coordinator {
    for (int i = 0; i < self.childCoordinators.count; i++) {
        NSObject<Coordinator> *coor = [self.childCoordinators objectAtIndex:i];
        if (coor == coordinator) {
            return;
        }
    }
    
    if (self.childCoordinators == NULL) {
        self.childCoordinators = [[NSMutableArray<Coordinator> alloc] init];
    }
    
    [self.childCoordinators addObject:coordinator];
    NSLog(@"The child coors: %@", self.childCoordinators);
}

- (void)removeDependency:(NSObject<Coordinator> * _Nullable)coordinator {
    if (self.childCoordinators != NULL && coordinator != NULL) {
        [self.childCoordinators removeObject:coordinator];
    }
}

@end
