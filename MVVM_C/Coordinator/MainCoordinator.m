//
//  MainCoordinator.m
//  MVVM_C
//
//  Created by Zoe.Lin on 2019/10/16.
//  Copyright © 2019 Zoe.Lin. All rights reserved.
//

#import "MainCoordinator.h"
#import "ProductListViewController.h"

@implementation MainCoordinator

@synthesize finishFlow;

- (void)start {
    [self showProductListVC];
}

// MARK - private function
- (void)showProductListVC {
    ProductListViewModel *vm = [[ProductListViewModel alloc] init];
    ProductListViewController *vc = [[ProductListViewController alloc] initWithViewModel:vm];
    
    __weak typeof(self) weakSelf = self;
    vc.logoutAction = ^{
        weakSelf.finishFlow();
    };
    
    [self.rootController presentViewController:vc animated:YES completion:nil];
}

@end
