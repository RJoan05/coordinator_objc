//
//  MainCoordinator.h
//  MVVM_C
//
//  Created by Zoe.Lin on 2019/10/16.
//  Copyright © 2019 Zoe.Lin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseCoordinator.h"

NS_ASSUME_NONNULL_BEGIN

@interface MainCoordinator : BaseCoordinator<CoordinatorFinishOutput>

@end

NS_ASSUME_NONNULL_END
