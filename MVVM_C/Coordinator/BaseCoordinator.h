//
//  BaseCoordinator.h
//  MVVM_C
//
//  Created by Zoe.Lin on 2019/10/8.
//  Copyright © 2019 Zoe.Lin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Coordinator.h"

NS_ASSUME_NONNULL_BEGIN

@interface BaseCoordinator : NSObject <Coordinator>

@property (nonatomic, strong) NSMutableArray<Coordinator> *childCoordinators;
@property (nonatomic, strong) UINavigationController *rootController;

- (instancetype)initWithRootViewController:(UINavigationController *)rootController;

- (void)addDependency:(NSObject<Coordinator> *)coordinator;

- (void)removeDependency:(NSObject<Coordinator> * _Nullable)coordinator;


@end

NS_ASSUME_NONNULL_END
