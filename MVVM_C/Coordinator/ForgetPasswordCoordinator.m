//
//  ForgetPasswordCoordinator.m
//  MVVM_C
//
//  Created by Zoe.Lin on 2019/10/15.
//  Copyright © 2019 Zoe.Lin. All rights reserved.
//

#import "ForgetPasswordCoordinator.h"
#import "ForgetPasswordViewModel.h"
#import "ForgetPasswordViewController.h"

@implementation ForgetPasswordCoordinator

@synthesize finishFlow;

- (void)start {
    [self showForgetPassword];
}

// MARK - private function
- (void)showForgetPassword {
    ForgetPasswordViewModel *vm = [[ForgetPasswordViewModel alloc] init];
    ForgetPasswordViewController *vc = [[ForgetPasswordViewController alloc] initWithViewModel:vm];
    __weak typeof(self) weakSelf = self;
    vc.sendPasswordAction = ^{
        [weakSelf showNewPassword];
    };
    
    [self.rootController pushViewController:vc animated:YES];
}

- (void)showNewPassword {
    UIAlertController *aController = [UIAlertController alertControllerWithTitle:@"New Password" message:@"123456" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"Confirm" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.finishFlow();
    }];
    [aController addAction:confirmAction];
    [self.rootController presentViewController:aController animated:YES completion:nil];
}

@end
