//
//  ApplicationCoordinator.m
//  MVVM_C
//
//  Created by Zoe.Lin on 2019/10/8.
//  Copyright © 2019 Zoe.Lin. All rights reserved.
//

#import "ApplicationCoordinator.h"
#import "AuthCoordinator.h"
#import "MainCoordinator.h"

typedef NS_ENUM(NSInteger, LaunchInstructor) {
    launchMain = 1,
    launchAuth,
    launchOnboarding
};

@interface ApplicationCoordinator ()

@property (nonatomic, assign) BOOL tutorialWasShown;
@property (nonatomic, assign) BOOL isAutorized;


@end

@implementation ApplicationCoordinator

// MARK - Coordinator protocol
- (void)start:(DeepLinkOption)option {
    // Override
    NSLog(@"ApplicationCoordinator start");
    if (option > none) {
        
    } else {
        switch ([self getLaunchInterface]) {
            case launchMain: {
                [self runMainFlow];
            }
                break;
            case launchAuth: {
                [self runAuthFlow];
            }
                break;
            case launchOnboarding: {
                [self runAuthFlow];
            }
                break;

            default:
                [self runAuthFlow];
                break;
        }
    }
}

// MARK - private
- (void)runAuthFlow {
    AuthCoordinator *coor = [[AuthCoordinator alloc] initWithRootViewController:self.rootController];
    
    __weak AuthCoordinator *weakCoor = coor;
    __weak typeof(self) weakSelf = self;
    coor.finishFlow = ^{
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf removeCoordinatorDependencyAndRestart:weakCoor isAutorized:TRUE tutorialWasShown:TRUE];
    };
    
    [self addDependency:coor];
    [coor start];
}

- (void)runMainFlow {
    MainCoordinator *coor = [[MainCoordinator alloc] initWithRootViewController:self.rootController];
    
    __weak MainCoordinator *weakCoor = coor;
    __weak typeof(self) weakSelf = self;
    coor.finishFlow = ^{
        __strong typeof(self) strongSelf = weakSelf;
        [strongSelf removeCoordinatorDependencyAndRestart:weakCoor isAutorized:FALSE tutorialWasShown:FALSE];
    };
    
    [self addDependency:coor];
    [coor start];
}

- (void)runOnboardingFlow {
    NSLog(@"run Onboarding Flow ");
}

- (void)removeCoordinatorDependencyAndRestart:(BaseCoordinator *)coor isAutorized:(BOOL)isAutorized tutorialWasShown:(BOOL)tutorialWasShown {
    NSLog(@"Here is resetting the configure about login launch.");
    [self removeDependency:coor];
    self.isAutorized = isAutorized;
    self.tutorialWasShown = tutorialWasShown;
    [self start];
}

- (LaunchInstructor)getLaunchInterface {
    if (_tutorialWasShown && _isAutorized) {
        return launchMain;
    } else if (!_tutorialWasShown && _isAutorized) {
        return launchOnboarding;
    }
    
    return launchAuth;
}



@end
