//
//  AuthCoordinator.m
//  MVVM_C
//
//  Created by Zoe.Lin on 2019/10/9.
//  Copyright © 2019 Zoe.Lin. All rights reserved.
//

#import "AuthCoordinator.h"
#import "LoginViewController.h"
#import "LoginViewModel.h"
#import "ForgetPasswordCoordinator.h"

@implementation AuthCoordinator
// MARK - CoordinatorFinishOutput
@synthesize finishFlow;

// override
- (void)start {
    [self showLoginViewController];
}

// MARK: - Private
- (void)showLoginViewController {
    LoginViewModel *vm = [[LoginViewModel alloc] init];
    LoginViewController *vc = [[LoginViewController alloc] initWithViewModel:vm];
    __weak typeof(self) weakSelf = self;
    vc.loginAction = ^{
        __strong typeof (weakSelf) sSelf = weakSelf;
        sSelf.finishFlow();
    };
    
    vc.forgetPwAction = ^{
        __strong typeof (weakSelf) sSelf = weakSelf;
        [sSelf showForgetPassword];
    };
    
    self.rootController.viewControllers = @[vc];
}

- (void)showForgetPassword {
    ForgetPasswordCoordinator *coor = [[ForgetPasswordCoordinator alloc] initWithRootViewController:self.rootController];
    __weak ForgetPasswordCoordinator *weakCoor = coor;
    __weak typeof(self) weakSelf = self;
    coor.finishFlow = ^{
        [weakSelf removeDependency:weakCoor];
        [weakSelf.rootController popViewControllerAnimated:YES];
    };
    
    [self addDependency:coor];
    [coor start];
}


@end
