//
//  Coordinator.h
//  MVVM_C
//
//  Created by Zoe.Lin on 2019/10/9.
//  Copyright © 2019 Zoe.Lin. All rights reserved.
//

#ifndef Coordinator_h
#define Coordinator_h

typedef NS_ENUM(NSInteger, DeepLinkOption) {
    none = 0,
    auth = 1,
    main
};

@protocol Coordinator

@property (nonatomic, strong) NSMutableArray *childCoordinators;
@required
- (void)start;
- (void)start:(DeepLinkOption)option;

@end

@protocol CoordinatorFinishOutput

typedef void(^FinishBlock)(void);
@required
@property (nonatomic, copy) FinishBlock finishFlow;

@end

#endif /* Coordinator_h */
