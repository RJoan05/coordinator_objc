//
//  CoordinatorFactoryProtocol.h
//  MVVM_C
//
//  Created by Zoe.Lin on 2019/10/9.
//  Copyright © 2019 Zoe.Lin. All rights reserved.
//

#ifndef CoordinatorFactoryProtocol_h
#define CoordinatorFactoryProtocol_h

@protocol CoordinatorFactoryProtocol

- (AuthCoordinator)makeAuthCoordinatorBox:(RouterProtocol)router coordinatorFactory(CoordinatorFactoryProtocol)coordinatorFactory viewControllerFactory(ViewControllerFactory):viewControllerFactory;


@end

#endif /* CoordinatorFactoryProtocol_h */
