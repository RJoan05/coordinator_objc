//
//  AppDelegate.h
//  MVVM_C
//
//  Created by Zoe.Lin on 2019/10/8.
//  Copyright © 2019 Zoe.Lin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UINavigationController *navigationController;
@property (strong, nonatomic) UIWindow *window;

@end

